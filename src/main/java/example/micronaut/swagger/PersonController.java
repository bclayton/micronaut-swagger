package example.micronaut.swagger;

import javax.annotation.Nullable;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(
    name = "person",
    description = "New person")
@Controller("/person")
public class PersonController {

  @Get(uri = "/{first}/{last}/{age}", produces = MediaType.APPLICATION_JSON)
  public HttpResponse<Response> person(
      @Nullable @PathVariable String first,
      @Nullable @PathVariable String last,
      @Nullable @PathVariable Integer age) {
    Response response = new Response(first, last, age);
    return HttpResponse.ok(response);
  }

  @Schema(name = "Response", description = "The new person")
  static class Response {
    String first;
    String last;
    int age;
    String name;

    Response(String first, String last, int age) {
      this.first = first;
      this.last = last;
      this.age = age;
      this.name = first + " " + last;
    }

    @Schema(name = "First name")
    public String getFirst() {
      return first;
    }

    @Schema(name = "Last name")
    public String getLast() {
      return last;
    }

    @Schema(name = "Age")
    public int getAge() {
      return age;
    }

    @Schema(name = "Full name")
    public String getName() {
      return name;
    }
  }

}
