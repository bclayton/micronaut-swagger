package example.micronaut.swagger;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(
    name = "hello",
    description = "Hello world")
@Controller("/hello")
public class HelloController {

  @Get(uri = "/", produces = MediaType.TEXT_PLAIN)
  @Operation(
      summary = "Say Hello World",
      description = "A Hello World example")
  @ApiResponse(content = @Content(
      mediaType = "text/plain",
      schema = @Schema(type = "string")))
  public String helloWorld() {
    return "Hello World";
  }

}
