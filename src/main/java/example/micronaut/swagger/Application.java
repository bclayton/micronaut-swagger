package example.micronaut.swagger;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
    info = @Info(
        title = "Hello World",
        version = "0.0",
        description = "Micronaut with Swagger"))
public class Application {

  public static void main(String[] args) {
    Micronaut.build(args)
        .mainClass(Application.class)
        .start();
  }

}
